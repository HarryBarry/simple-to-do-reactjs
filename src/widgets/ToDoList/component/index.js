import React, {Component} from "react";
import {TodoForm} from '../../ToDoForm';
import CSSModules from 'react-css-modules';

import style from '../scss/index.scss';
@CSSModules(style, {
  allowMultiple: true
})

class ToDoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        'Establish the addition of new tasks in the TODO List',
        'Prevent adding empty elements',
        'Move the focus on the input of a new task at startup. And transferred to the same focus with the addition of each new task to the list',
        'Implement the function of the task complete'
      ]};

  }

  updateItems (newItem) {
    if (newItem != '')
      this.setState({
        items: this.state.items.concat(newItem)
      });
  }

  renderListItems(items) {
    let listItems = items.map((item, index) => {
      return (
        <li key={index} styleName="list-item">
          {item}
        </li>
      )
    });

    return (
        <ol>
          {listItems}
        </ol>
    )
  }

  render () {
    return (
        <div styleName="todo">
          <h3 styleName='todo__banner'>TODO List</h3>
          { this.renderListItems(this.state.items) }
          <TodoForm onFormSubmit={::this.updateItems}/>
        </div>
    );
  }
}

export default ToDoList;
