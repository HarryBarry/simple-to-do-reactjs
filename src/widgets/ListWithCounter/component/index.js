import React, {Component} from "react";
import Counter from '../../Counter/component';

class ListCounter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        { id: 2, name: 'item'},
        { id: 1, name: 'item'},
        { id: 0, name: 'item'}
      ]
    }
  }

  appendItem = () => {
    this.state.data.unshift({
      id: this.state.data.length,
      name: 'item'
    });

    this.setState({'data': this.state.data});
  };

  renderListItems(items) {
    return items.map((item) => {
      return (<li key={item.id}>{item.id} - {item.name} - <Counter/></li>);
    })
  }

  render() {
    return (
      <div>
        <p>1. The counter for each item has to work by pressing the "+" button in the element</p>
        <p>2. When adding a new element, the counter stores in the item that has been created</p>
        <ul>
          {this.renderListItems(this.state.data)}
        </ul>
        <button onClick={()=> this.appendItem() }>add item</button>
      </div>
    )
  }
}

export default ListCounter;