import * as types from "../constant";

const initialState = {
  currentFilter: (localStorage.getItem('currentFilter'))?localStorage.getItem('currentFilter'):'all',
  tasks: []
};

export default function (state = initialState, action) {
  switch (action.type) {
    case types.GET_TASKS:
      return {...state, tasks: action.payload };
    case types.ADD_TASK:
      return {...state, tasks: action.payload };
    case types.REMOVE_TASK:
      return {...state, tasks: action.payload };
    case types.COMPLETED_TASK:
      return {...state, tasks: action.payload };
    case types.FILTER_CHANGE:
      return {...state, tasks: action.payload.updatedTasks, currentFilter: action.payload.currentFilter };
    default:
      return state;
  }
}
