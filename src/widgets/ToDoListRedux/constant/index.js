export const GET_TASKS = 'GET_TASKS';
export const ADD_TASK = 'ADD_TASK';
export const REMOVE_TASK = 'REMOVE_TASK';
export const COMPLETED_TASK = 'COMPLETED_TASK';
export const FILTER_CHANGE = 'FILTER_CHANGE';