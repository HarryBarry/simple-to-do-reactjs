import * as types from "../constant";

export function getTasks() {
  return dispatch => {
    let updatedTasks = (localStorage.getItem('tasks'))?JSON.parse(localStorage.getItem('tasks')):[];

    dispatch({
      type: types.GET_TASKS,
      payload: updatedTasks
    });
  }
}

export function addTask(tasks) {
  localStorage.setItem('tasks', JSON.stringify(tasks));
  let updatedTasks = (localStorage.getItem('tasks'))?JSON.parse(localStorage.getItem('tasks')):tasks;

  return dispatch => {
    dispatch({
      type: types.ADD_TASK,
      payload: updatedTasks
    });
  }
}

export function removeTask(tasks, index) {
  tasks.splice(index, 1);
  localStorage.setItem('tasks', JSON.stringify(tasks));
  let updatedTasks = (localStorage.getItem('tasks'))?JSON.parse(localStorage.getItem('tasks')):tasks;

  return dispatch => {
    dispatch({
      type: types.REMOVE_TASK,
      payload: updatedTasks
    });
  }
}

export function completeTask(tasks) {
  localStorage.setItem('tasks', JSON.stringify(tasks));
  let updatedTasks = (localStorage.getItem('tasks'))?JSON.parse(localStorage.getItem('tasks')):tasks;

  return dispatch => {
    dispatch({
      type: types.COMPLETED_TASK,
      payload: updatedTasks
    });
  }
}

export function changeFilter(currentFilter) {
  localStorage.setItem('currentFilter', currentFilter);
  let tasks = (localStorage.getItem('tasks'))?JSON.parse(localStorage.getItem('tasks')):[];
  let updatedTasks = (currentFilter !== 'all')?filterTasks(tasks, currentFilter):tasks;
  
  let data = {
    updatedTasks: updatedTasks,
    currentFilter: currentFilter
  };

  return dispatch => {
    dispatch({
      type: types.FILTER_CHANGE,
      payload: data
    });
  }
}

function filterTasks(tasks, currentFilter) {
  return tasks.filter((task) => {
    return task.complete == (currentFilter == 'completed');
  });
}