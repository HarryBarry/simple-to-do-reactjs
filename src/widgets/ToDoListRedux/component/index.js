import React, {Component} from "react";
import {connect} from "react-redux";
import {TodoForm} from '../../ToDoForm';
import CSSModules from 'react-css-modules';
import style from '../scss/index.scss';
import { getTasks, addTask, removeTask, completeTask, changeFilter } from '../action';



@connect( ({todo}) => ({...todo}), {getTasks, addTask, removeTask, completeTask, changeFilter} )
@CSSModules(style, {
  allowMultiple: true
})

class ToDoListRedux extends Component {
  constructor(props) {
    super(props);
  }

  updateItems (item) {
    const {tasks, addTask, changeFilter} = this.props;

    let newItem = {
      data: item,
      complete: false
    };

    let updatedTasks;
    if (newItem != '') {
      updatedTasks = tasks.concat(newItem);
      addTask(updatedTasks);
      changeFilter('all');
    }
  }
  
  
  renderListItems(items) {
    const {removeTask, completeTask, currentFilter} = this.props;
    
    let listItems = items.map((item, index) => {
      return (
        <li key={index} styleName="list-item">
          {(currentFilter == 'all')?<div><a onClick={()=>{ item.complete = !item.complete; completeTask(items); }}>({(!item.complete)?'complete':'un complete'})</a></div>:''}
          <div styleName="list-item-data">{item.data}</div>
          <div>(<a onClick={()=>{ removeTask(items, index) }}>remove</a>)</div>
        </li>
      )
    });

    return (
      <ol>
        {listItems}
      </ol>
    )
  }

  onFilterChange(filterName) {
    const {changeFilter} = this.props;
    changeFilter(filterName);
  }

  getItemClass(filterName) {
    const {currentFilter} = this.props;
    let styleName = 'todo__filter-item';

    if (currentFilter === filterName)
      styleName = `${styleName}--current`;

    return styleName;
  }

  renderFilter() {

    return (
        <div styleName='todo__filter'>Show:
        {['all', 'completed', 'uncompleted'].map((filterName, index) =>
            <div styleName={this.getItemClass(filterName)} key={index} onClick={() => ::this.onFilterChange(filterName)}>
              {filterName}
            </div>
        )}
        </div>
    )
  }

  componentWillMount() {
    const {getTasks, currentFilter, changeFilter} = this.props;
    getTasks();
    changeFilter(currentFilter);
  }

  render () {
    const {tasks} = this.props;

    if (!tasks)
      return (<div>Loading...</div>);

    return (
      <div styleName="todo">
        <h3 styleName='todo__banner'>TODO List</h3>
        {this.renderFilter()}
        { this.renderListItems(tasks) }
        <TodoForm onFormSubmit={::this.updateItems}/>
      </div>
    );
  }
}

export default ToDoListRedux;
