import React, {Component} from "react";

class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
  }

  handleClick = () => {
    this.setState({'count': this.state.count + 1});
  };

  render() {
    return (
      <span>
        {this.state.count}
        <button onClick={this.handleClick}>+</button>
      </span>
    )
  }
}

export default Counter;