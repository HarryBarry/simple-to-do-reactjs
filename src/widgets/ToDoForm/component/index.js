import React, {Component} from "react";
import CSSModules from 'react-css-modules';

import style from '../scss/index.scss';
@CSSModules(style, {
  allowMultiple: true
})

class TodoForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: ''
    }
  }

  handleSubmit (event) {
    event.preventDefault();
    this.props.onFormSubmit(this.state.item);
    this.setState({
      item: ''
    });
    this.inputTask.focus();

    return false;
  }

  onChange (e) {
    this.setState({
      item: e.target.value
    });
  }

  componentDidMount() {
    this.inputTask.focus();
  }

  render () {
    return (
      <form styleName='todo__form'
            onSubmit={::this.handleSubmit}>
        <input
          styleName='todo__form-input'
          type='text'
          ref={(input) => { this.inputTask = input; }}
          onChange={::this.onChange}
          value={this.state.item} />
        <input
          styleName='todo__form-add'
          type='submit'
          value='Add'
        />
      </form>
    );
  }
}

export default TodoForm;