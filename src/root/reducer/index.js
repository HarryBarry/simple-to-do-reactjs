import {combineReducers} from "redux";

import {todo} from '../../widgets';

const rootReducer = combineReducers({
  todo
});

export default rootReducer;
