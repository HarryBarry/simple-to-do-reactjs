import React, {Component} from "react";
import {connect} from "react-redux";
import {ListCounter, ToDoList, ToDoListRedux} from '../../../../widgets';

class Home extends Component {

    render() {
        return (
            <div>
              <h1>First Task</h1>
              <ListCounter/>

              <h1>Second Task</h1>
              <ToDoList/>

              <h1>Third Task</h1>
              <ToDoListRedux/>
              
            </div>
        )
    }
}

export default Home;

